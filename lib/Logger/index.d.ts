declare type Level = 'error' | 'warn' | 'info' | 'debug';
declare type LevelMap = {
    [L in Level]: number;
};
interface Item {
    reference?: string;
    message: string;
    metadata?: Metadata;
}
interface Metadata {
    [key: string]: unknown;
}
export default class Logger {
    static LEVELS_MAP: LevelMap;
    protected level: number;
    namespace?: string;
    metadata?: Metadata;
    constructor(metadata?: Metadata, level?: number | Level);
    private static nameToNumber;
    private static numberToName;
    private write;
    error(logItem: Item): void;
    warn(logItem: Item): void;
    info(logItem: Item): void;
    debug(logItem: Item): void;
    setNamespace(namespace: string): void;
    setMetadata(metadata: Metadata): void;
}
export {};
