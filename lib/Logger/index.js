"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Logger {
    constructor(metadata, level = 2) {
        if (typeof level === 'string') {
            this.level = Logger.LEVELS_MAP[level];
        }
        else {
            this.level = level;
        }
        this.metadata = metadata;
    }
    static nameToNumber(name) {
        return Logger.LEVELS_MAP[name];
    }
    static numberToName(number) {
        for (const key of Object.keys(Logger.LEVELS_MAP)) {
            if (Logger.LEVELS_MAP[key] === number) {
                return key;
            }
        }
    }
    write(level, logItem) {
        let method;
        if (typeof level === 'number') {
            method = Logger.numberToName(level) || 'info';
        }
        else if (['error', 'warn', 'info', 'debug'].indexOf(level) !== -1) {
            method = level;
        }
        else {
            method = 'info';
        }
        if (Logger.nameToNumber(method) < this.level) {
            return;
        }
        if (this.namespace) {
            if (logItem.reference) {
                logItem.reference = `${this.namespace}.${logItem.reference}`;
            }
            else {
                logItem.reference = this.namespace;
            }
        }
        if (this.metadata) {
            logItem.metadata = Object.assign(Object.assign({}, this.metadata), logItem.metadata);
        }
        console[method](JSON.stringify(logItem, null, 2));
    }
    error(logItem) {
        this.write('error', logItem);
    }
    warn(logItem) {
        this.write('warn', logItem);
    }
    info(logItem) {
        this.write('info', logItem);
    }
    debug(logItem) {
        this.write('debug', logItem);
    }
    setNamespace(namespace) {
        if (typeof namespace !== 'string') {
            throw new Error('Logging namespace must be a string');
        }
        this.namespace = namespace;
    }
    setMetadata(metadata) {
        this.metadata = metadata;
    }
}
exports.default = Logger;
Logger.LEVELS_MAP = {
    error: 4,
    warn: 3,
    info: 2,
    debug: 1,
};
//# sourceMappingURL=index.js.map