type Level = 'error' | 'warn' | 'info' | 'debug';
type LevelMap = {
    [L in Level]: number;
};

interface Item {
    reference?: string;
    message: string;
    metadata?: Metadata;
}
interface Metadata {
    [key: string]: unknown;
}

export default class Logger {
    static LEVELS_MAP = {
        error: 4,
        warn: 3,
        info: 2,
        debug: 1,
    } as LevelMap;
    protected level: number;
    namespace?: string;
    metadata?: Metadata;

    constructor(metadata?: Metadata, level: number | Level = 2) {
        if (typeof level === 'string') {
            this.level = Logger.LEVELS_MAP[level];
        } else {
            this.level = level;
        }
        this.metadata = metadata;
    }

    private static nameToNumber(name: Level): number {
        return Logger.LEVELS_MAP[name];
    }

    private static numberToName(number: number): Level | void {
        for (const key of Object.keys(Logger.LEVELS_MAP) as Level[]) {
            if (Logger.LEVELS_MAP[key] === number) {
                return key as Level;
            }
        }
    }

    private write(level: Level | number, logItem: Item): void {
        let method: Level;
        if (typeof level === 'number') {
            method = Logger.numberToName(level) || 'info';
        } else if (['error', 'warn', 'info', 'debug'].indexOf(level) !== -1) {
            method = level;
        } else {
            method = 'info';
        }
        if (Logger.nameToNumber(method) < this.level) {
            return;
        }
        if (this.namespace) {
            if (logItem.reference) {
                logItem.reference = `${this.namespace}.${logItem.reference}`;
            } else {
                logItem.reference = this.namespace;
            }
        }
        if (this.metadata) {
            logItem.metadata = {
                ...this.metadata,
                ...logItem.metadata,
            };
        }
        console[method](JSON.stringify(logItem, null, 2));
    }
    public error(logItem: Item): void {
        this.write('error', logItem);
    }
    public warn(logItem: Item): void {
        this.write('warn', logItem);
    }
    public info(logItem: Item): void {
        this.write('info', logItem);
    }
    public debug(logItem: Item): void {
        this.write('debug', logItem);
    }
    public setNamespace(namespace: string): void {
        if (typeof namespace !== 'string') {
            throw new Error('Logging namespace must be a string');
        }
        this.namespace = namespace;
    }
    public setMetadata(metadata: Metadata): void {
        this.metadata = metadata;
    }
}
